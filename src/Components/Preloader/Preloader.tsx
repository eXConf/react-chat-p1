import React, { Component } from "react";
import "./preloader.css";

export default class Preloader extends Component {
  render() {
    return (
      <div className="preloader">
        <div className="lds-default">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    );
  }
}
