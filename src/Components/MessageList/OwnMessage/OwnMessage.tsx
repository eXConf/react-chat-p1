import React, { Component } from "react";
import { MessageType } from "../../../Types/MessageType";
import { getHoursMinutes } from "../../../helpers/dateTimeHelper";
import "./own-message.css";

type OwnMessageProps = {
  message: MessageType;
  onMessageEditClicked: Function;
  onDeleteMessageClicked: Function;
};

export default class OwnMessage extends Component<OwnMessageProps> {
  render() {
    const { createdAt, editedAt, text } = this.props.message;
    const time = editedAt ? editedAt : createdAt;
    const timeFormatted = getHoursMinutes(new Date(time));

    return (
      <div className="own-message">
        <div className="text-container">
          <div className="message-info">
            <div className="message-time">{timeFormatted}</div>
            <div className="controls">
              <div
                className="message-edit"
                onClick={() => this.onEditClicked()}
              >
                <i className="fas fa-pencil-alt"></i>
              </div>
              <div
                className="message-delete"
                onClick={() => this.onDeleteClicked()}
              >
                <i className="fas fa-trash-alt"></i>
              </div>
            </div>
          </div>
          <div className="message-text">{text}</div>
        </div>
      </div>
    );
  }

  onEditClicked() {
    const { onMessageEditClicked } = this.props;
    const { id, text } = this.props.message;
    onMessageEditClicked(id, text);
  }

  onDeleteClicked() {
    this.props.onDeleteMessageClicked(this.props.message.id);
  }
}
