import React, { Component } from "react";
import { MessageType } from "../../Types/MessageType";
import "./message-input.css";

type MessageInputProps = {
  postMessage: Function;
  editMessage: Function;
  setInputtext: Function;
  inputText: string;
  user: string;
  userId: string;
  editPostId: string;
  inputRef: React.RefObject<HTMLInputElement>;
};

export default class MessageInput extends Component<MessageInputProps> {
  render() {
    return (
      <div className="message-input">
        <input
          value={this.props.inputText}
          ref={this.props.inputRef}
          onChange={e => this.props.setInputtext(e.target.value)}
          onKeyUp={e => this.onInputKeyPressed(e)}
          type="text"
          className="message-input-text"
        />
        <button
          onClick={() => this.postMessage()}
          className="message-input-button"
        >
          Send
        </button>
      </div>
    );
  }

  onInputKeyPressed(e: React.KeyboardEvent) {
    if (e.key === "Enter") {
      this.postMessage();
    }
  }

  postMessage() {
    const {
      editPostId,
      inputText,
      user,
      userId,
      postMessage,
      editMessage,
      inputRef,
    } = this.props;
    const message: MessageType = {
      avatar: "",
      createdAt: new Date().toString(),
      editedAt: "",
      id: editPostId ? editPostId : Date.now().toString(),
      text: inputText,
      user: user,
      userId: userId,
    };

    if (editPostId) {
      editMessage(message);
    } else {
      postMessage(message);
      inputRef.current?.focus();
    }

    inputRef.current?.focus();
  }
}
