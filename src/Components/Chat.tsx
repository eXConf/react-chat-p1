import React, { Component } from "react";
import Header from "./Header/Header";
import MessageList from "./MessageList/MessageList";
import MessageInput from "./MessageInput/MessageInput";
import { MessageType } from "../Types/MessageType";
import "./chat.css";
import Preloader from "./Preloader/Preloader";

export type ChatProps = { url: string };
export type ChatState = {
  messages: MessageType[];
  userId: string;
  user: string;
  inputText: string;
  editPostId: string;
};

export default class Chat extends Component<ChatProps, ChatState> {
  state = {
    messages: [],
    userId: "4b003c20-1b8f-11e8-9629-c7eca82aa7bd",
    user: "Helen",
    inputText: "",
    editPostId: "",
  };

  inputRef = React.createRef<HTMLInputElement>();

  async componentDidMount(): Promise<void> {
    const data = await this.fetchData(this.props.url);
    const messages = this.sortMessagesByDate(data);

    this.setState({
      messages,
    });
  }

  render() {
    return this.state.messages.length > 0 ? (
      <div className="chat" style={{ margin: "20px 50px" }}>
        <Header
          usersCount={this.getUsersCount()}
          messagesCount={this.getMessagesCount()}
          lastMessageTime={this.getLastMessageTime()}
        />
        <MessageList
          messages={this.state.messages}
          userId={this.state.userId}
          onMessageEditClicked={(id: string, text: string) =>
            this.onMessageEditClicked(id, text)
          }
          onDeleteMessageClicked={(id: string) =>
            this.onDeleteMessageClicked(id)
          }
        />
        <MessageInput
          postMessage={(message: MessageType) => this.postMessage(message)}
          editMessage={(message: MessageType) => this.editMessage(message)}
          setInputtext={(text: string) => this.setInputText(text)}
          inputText={this.state.inputText}
          userId={this.state.userId}
          user={this.state.user}
          editPostId={this.state.editPostId}
          inputRef={this.inputRef}
        />
      </div>
    ) : (
      <Preloader />
    );
  }

  async fetchData(url: string): Promise<MessageType[]> {
    const response = await fetch(url);
    const data: MessageType[] = await response.json();

    return data;
  }

  getUsersCount(): number {
    const { messages } = this.state;
    const users = messages.map((message: MessageType) => message.userId);
    const unique = new Set(users);

    return unique.size;
  }

  getMessagesCount(): number {
    const { messages } = this.state;
    return messages.length;
  }

  getLastMessageTime(): Date | null {
    const { messages }: { messages: MessageType[] } = this.state;

    if (messages.length === 0) return null;

    const time = messages[messages.length - 1].createdAt;
    return new Date(time);
  }

  sortMessagesByDate(data: MessageType[]): MessageType[] {
    const messages = [...data];

    messages.sort((a: MessageType, b: MessageType) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
    });

    return messages;
  }

  postMessage(message: MessageType) {
    this.setState(
      prev => ({
        messages: [...prev.messages, message],
      }),
      () => {
        if (window) {
          window?.scrollTo(0, document.body.scrollHeight);
        }
      }
    );
    this.setInputText("");
  }

  editMessage(message: MessageType) {
    this.setState(prev => {
      const newMessages = prev.messages.map(el => {
        const newMessage = { ...el };
        if (el.id === message.id) {
          newMessage.text = message.text;
        }
        return newMessage;
      });
      return { messages: newMessages };
    });
    this.setState({ editPostId: "" });
    this.setInputText("");
  }

  setInputText(text: string) {
    this.setState({ inputText: text });
  }

  onMessageEditClicked(id: string, text: string) {
    this.setState({ editPostId: id });
    this.setInputText(text);
    this.inputRef.current?.focus();
  }

  onDeleteMessageClicked(id: string) {
    this.setState(prev => {
      const messages = prev.messages.filter(message => message.id !== id);
      return { messages };
    });
  }
}
